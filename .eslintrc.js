module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
    node: true,
    jest: true,
  },
  extends: ["next/babel", "next/core-web-vitals"],
  parserOptions: {
    ecmaVersion: "latest",
  },
  rules: {},
};
